import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Semaphore;

public class Global {
    public static CountDownLatch latch_1 = new CountDownLatch(4);
    public static CountDownLatch latch_2 = new CountDownLatch(4);
    public static CountDownLatch latch_3 = new CountDownLatch(4);
    public static CountDownLatch latch_4 = new CountDownLatch(4);
    public static CountDownLatch latch_5 = new CountDownLatch(4);
}