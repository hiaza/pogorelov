// синхронізувати курсори потоків(створювати курсор для кожного потоку і
// перед кожною операцією синхронізовувати курсори потоку і фс)

import java.util.*;
import java.util.concurrent.Semaphore;

public class MyFileSystem {

    private Tree<FSElement>fs;
    public Node<FSElement> root;
    public Node<FSElement> cursor;
    public Node<FSElement> buff;

    public static Semaphore cursor_freeze = new Semaphore(1, true);

    public MyFileSystem() {
        fs = new Tree<FSElement>(new Directory("Home"));
        cursor = fs.root;
        root = fs.root;
    }

    public static void main(String[] args) {
        MyFileSystem system = new MyFileSystem();

        system.CreateFile(new BinaryFile("bin_file","artttttttttttem"));
        system.CreateFile(new LogFile("log_file","sdfsdfsdf"));
        system.CreateFile(new BufferFile("buff_file"));
        system.CreateFile(new ConfigurationFile("System.config"));

        system.cursor = system.cursor.children.get(3);
        system.addConfig("NumOfCursors",1);
        system.addConfig("NumOfUsers",1);
        system.cursor = system.root;

        system.createDirectory("Directory");
        system.getFiles();
        system.cursor = system.cursor.children.get(4);
        system.getCurrentLocation();

        system.createDirectory("Directory_in_directory");
        system.CreateFile(new BinaryFile("bin_file2","artttttttttttem"));
        system.CreateFile(new LogFile("log_file2","sdfsdfsdf"));
        system.CreateFile(new BufferFile("buff_file2"));
        system.getFiles();

        system.cursor = system.cursor.children.get(0);
        system.CreateFile(new BinaryFile("bin_file3","artttttttttttem"));
        system.getCurrentLocation();

        system.Copy();
        system.cursor = system.root;
        system.MoveFile();
        system.getFiles();
    }

    interface FSElement{
        public String getName();
    }
    interface Readble{
        public String getContent();
    }


    abstract static class SimpleFile implements FSElement{
        private String name;
        public void setName(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

    }

    public String getCurrentElementName(Node<FSElement> element){
        return element.data.getName();

    }

    public List<String> getFiles(){
        try
        {
            cursor_freeze.acquire();
            if(cursor.data.getClass() == Directory.class){
                List<String> files = new ArrayList<String>();
                System.out.println("Directory "+cursor.data.getName()+" contains:");

                if(cursor.children.size()<1){
                    System.out.println("Пусто!");
                    return null;
                }
                for (Node<FSElement>node:cursor.children) {
                    String name = node.data.getName();
                    System.out.println("|--"+name+": (" + node.data.getClass().getSimpleName()+")");
                    files.add(name);
                }
                return files;
            }
            else {
                System.out.println("This is not directory");
                return null;
            }
        } catch(InterruptedException e) {
            System.out.println("Thread was interrupted");
            return null;
        }finally {
            cursor_freeze.release();
        }
    }

    public void getCurrentLocation(){
        try
        {
            String path = "";
            cursor_freeze.acquire();
            Node<FSElement>temp = cursor;
            do{
                path = "/"+getCurrentElementName(temp)+path;
                temp = temp.parent;
            }
            while(temp!=null);
            System.out.print("Path to "+cursor.data.getName()+":  ");
            System.out.println(path);
        } catch(InterruptedException e)
        {
            System.out.println("Thread was interrupted");
        }finally {
            cursor_freeze.release();
        }
    }


    public boolean CreateFile(SimpleFile temp){
        try{
            cursor_freeze.acquire();
            Node<FSElement> file = new Node<FSElement>(temp,cursor);
            if(cursor.data.getClass() == Directory.class){
                if(((Directory) cursor.data).add(temp.getName())){
                    cursor.children.add(file);
                    file.parent = cursor;
                    return true;
                }
                else{
                    System.out.println("Element with the same name is located here, or you're reached the max elems in dir");
                    return false;
                }
            }
            else {
                System.out.println("Can't create file here!");
                return false;
            }
        }
        catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }


    }

    boolean DeleteFile(){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass().getSuperclass() == SimpleFile.class) {
                Node<FSElement> parent = cursor.parent;
                parent.children.remove(cursor);
                cursor = parent;
                return true;
            } else {
                System.out.println("Can't delete file!");
                return false;
            }
        }
        catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    boolean Copy(){
        try {
            cursor_freeze.acquire();
            if (cursor.parent != null) {
                buff = new Node<FSElement>(cursor.data, cursor.parent);
                for (Node<FSElement> child : cursor.children) {
                    buff.children.add(child);
                }
                return true;
            }
            System.out.println("Can't copy the home directory");
            return false;
        }
        catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    synchronized boolean MoveFile(){
        if(buff==null){
            System.out.println("Choose file firstly");
            return false;
        }
        else{
            if(buff.parent!=null){
                Node<FSElement> parent = buff.parent;
                for (Node<FSElement> child:parent.children
                ) {
                    if(child.data.getName()==buff.data.getName()){
                        parent.children.remove(child);
                        break;
                    }
                }
            }
            try {
                cursor_freeze.acquire();
                if (cursor.data.getClass() == Directory.class) {
                    buff.parent = cursor;
                    cursor.children.add(buff);
                    for (Node<FSElement> child : buff.children) {
                        child.parent = buff;
                    }
                    System.out.println("Moved");
                    return true;
                } else {
                    System.out.println("Choose a directory");
                    return false;
                }
            }catch (InterruptedException e){
                    System.out.println("Thread was interrupted");
                    return false;
                }
            finally {
                    cursor_freeze.release();
                }
        }
    }

    boolean ReadFile(){
        try {
            cursor_freeze.acquire();
            System.out.println("Trying to read " + cursor.data.getName() + ": ");
            if (cursor.data.getClass().getInterfaces()[0] == Readble.class) {
                Readble temp = (Readble) cursor.data;
                System.out.println(temp.getContent());
                return true;
            } else {
                System.out.println("Can't read this");
                return false;
            }

        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    };

    boolean AppendLine(String line){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass() == LogFile.class) {
                LogFile temp = (LogFile) cursor.data;
                temp.appendLine(line);
                return true;
            } else {
                System.out.println("Can't append line to it");
                return false;
            }

        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

        public static class ConfigurationFile extends SimpleFile implements Readble{
            private Map<String,Integer> content = new HashMap<String,Integer>();

            public ConfigurationFile(String name) {
                    setName(name);
            }
            public void addConfig(String field, int value){
                if(content.containsKey(field)) content.replace(field,value);
                else content.put(field, value);

            }
            public int getConfig(String field){
                if(content.containsKey(field)) return content.get(field);
                else return -1;
            }
            public boolean deleteConfig(String field){
                if(content.containsKey(field)) {
                    content.remove(field);
                    return true;
                }
                else return false;
            }

            public String getContent() {
                String result = "";
                for(Map.Entry<String, Integer> entry: content.entrySet()) {
                    result+= entry.getKey() + " : " +entry.getValue() + "\n";
                }
                return result;
            }
        }


        public static class BinaryFile extends SimpleFile implements Readble{

        private String content;

        public BinaryFile(String name,String content) {
             setName(name);setContent(content);
        }

        public String getContent() {
            return content;
        }

        private void setContent(String content) {
            this.content = content;
        }
    }

    public static class LogFile extends SimpleFile implements Readble{
        private String content;

        public LogFile(String name,String content) {
            setName(name);setContent(content);
        }

        public String getContent() {
            return content;
        }

        private void setContent(String content) {
            this.content = content;
        }

        public boolean appendLine(String line){
            setContent(getContent() + "\n" +line);
            return true;
        }
    }


    public static class BufferFile extends SimpleFile{

        private Queue <FSElement> content = new LinkedList<FSElement>();
        public static final int MAX_BUF_FILE_SIZE = 6;

        public BufferFile(String name) {
            setName(name);
        }

        public boolean PushElement(FSElement element){
            if (content.size()<MAX_BUF_FILE_SIZE){
                content.add(element);
                return true;
            }
            System.out.println("Files Limit is reached");
            return false;
        }

        public FSElement ConsumeElement(){
            return content.poll();
        }
    }

    public boolean addConfig(String field, int value){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass() == ConfigurationFile.class) {
                ((ConfigurationFile) cursor.data).addConfig(field,value);
                System.out.println("Added");
                return true;
            } else {
                System.out.println("Choose a ConfigurationFile");
                return false;
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    public boolean deleteConfig(String field){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass() == ConfigurationFile.class) {
                boolean result = ((ConfigurationFile) cursor.data).deleteConfig(field);
                    if(result) System.out.println("Deleted Configuration");
                    else System.out.println("Configuration missed");
                return result;
            } else {
                System.out.println("Choose a ConfigurationFile");
                return false;
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    public boolean PushElement(){
        try {
            cursor_freeze.acquire();
            if (buff == null) {
                System.out.println("Choose file firstly");
                return false;
            } else {
                if (cursor.data.getClass() == BufferFile.class) {
                    ((BufferFile) cursor.data).PushElement(buff.data);
                    System.out.println("Pushed");
                    return true;
                } else {
                    System.out.println("Choose a BufferFile");
                    return false;
                }
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    public boolean ConsumeElement(){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass() == BufferFile.class) {
                buff = new Node<FSElement>(((BufferFile) cursor.data).ConsumeElement());
                System.out.println("Pushed");
                return true;
            } else {
                System.out.println("Choose a BufferFile");
                return false;
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
     }


    public static class Directory implements FSElement{
        public String name;
        public static final int DIR_MAX_ELEMS = 10;
        public int currentNumOfElems;
        public HashSet<String> fileNames = new HashSet<String>();
        public Directory(String name) {
            this.name = name;
        }
        public boolean hasSpace(){
           return currentNumOfElems < DIR_MAX_ELEMS;
        }
        public boolean add(String name){
            if(hasSpace() && fileNames.add(name)){
                currentNumOfElems++;
                return true;
            }
            else return false;
        }
        public String getName(){
            return name;
        }
    }

    public boolean createDirectory(String name){
        try {
            cursor_freeze.acquire();
            if(cursor.data.getClass() == Directory.class){
                if(((Directory) cursor.data).add(name)){
                    cursor.children.add(new Node<FSElement>(new Directory(name),cursor));
                    return true;
                }
                else{
                    System.out.println("Element with the same name is located here, or you're reached the max elems in dir");
                    return false;
                }
            }
            else {
                System.out.println("Can't create file here!");
                return false;
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }

    public boolean deleteDirectory(){
        try {
            cursor_freeze.acquire();
            if (cursor.data.getClass() == Directory.class) {
                if (cursor.parent == null) {
                    System.out.println("Can't delete root directory");
                    return false;
                }
                Node<FSElement> parent = cursor.parent;
                parent.children.remove(cursor);
                cursor.parent = null;
                cursor = parent;
                return true;
            } else {
                System.out.println("This is not directory!");
                return false;
            }
        }catch (InterruptedException e){
            System.out.println("Thread was interrupted");
            return false;
        }
        finally {
            cursor_freeze.release();
        }
    }
}
