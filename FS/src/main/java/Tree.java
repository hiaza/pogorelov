import java.util.ArrayList;
import java.util.List;

public class Tree <T> {
    public Node<T> root;

    public Tree(T rootDate) {
        root = new Node<T>(rootDate);
    }

}
