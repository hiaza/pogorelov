import java.util.ArrayList;
import java.util.List;

public class Node<T>{
    public T data;
    public Node<T> parent;
    public List<Node<T>> children;

    public Node(T data) {
        this.data = data;
        this.parent = null;
        this.children = new ArrayList<Node<T>>();
    }

    public Node(T data, Node<T> parent) {
        this.data = data;
        this.parent = parent;
        this.children = new ArrayList<Node<T>>();
    }
}