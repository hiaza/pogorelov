import java.util.concurrent.CountDownLatch;

public class Task3 {
    Task3(){
    }

    public static CountDownLatch latch_1_2 = new CountDownLatch(2);
    public static CountDownLatch latch_5 = new CountDownLatch(4);

    public static void main(String[] args) {
        /* Створюються потоки. */
        Thread_1 thread_1 = new Thread_1();
        Thread_2 thread_2 = new Thread_2();
        Thread_3 thread_3 = new Thread_3();
        Thread_4 thread_4 = new Thread_4();
        Thread_5 thread_5 = new Thread_5();

        /* Очікується завершення потоків. */
        try
        {
            /* Очікується завершення потоків. */
            thread_1.t.join();
            thread_2.t.join();
            thread_3.t.join();
            thread_4.t.join();
            thread_5.t.join();
        }catch(InterruptedException e)
        {
            System.out.println("Main Interrupted");
        }
    }

    public static class Thread_1 implements Runnable {
    Thread t;
        Thread_1() {
             t = new Thread(this, "Thread_1");
            this.t.start();
        }

        public void run() {
            task_initialise();
            task();
            task_finalise();
        }

        public void task_initialise(){

            try{
                t.sleep(1000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task initialisation");

            Global.latch_2.countDown();
            Global.latch_3.countDown();
            Global.latch_4.countDown();
            Global.latch_5.countDown();

            try
            {
                Global.latch_1.await();

            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }

        public void task(){
            try{
                t.sleep(2000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task");
            try
            {
                latch_1_2.await();
            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }
        public void task_finalise(){
            try{
                t.sleep(3000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task_finalise");
            latch_5.countDown();
        }
    }

    public static class Thread_2 implements Runnable {
        Thread t;
        Thread_2() {
            t = new Thread(this, "Thread_2");
            this.t.start();
        }

        public void run() {
            task_initialise();
            task();
            task_finalise();
        }

        public void task_initialise(){

            try{
                t.sleep(500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task initialisation");

            Global.latch_1.countDown();
            Global.latch_3.countDown();
            Global.latch_4.countDown();
            Global.latch_5.countDown();
            try
            {
                Global.latch_2.await();
            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }

        public void task(){
            try{
                t.sleep(1000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task");
            try
            {
                latch_1_2.await();
            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }
        public void task_finalise(){
            try{
                t.sleep(1500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task_finalise");
            latch_5.countDown();
        }
    }
    public static class Thread_3 implements Runnable {
        Thread t;
        Thread_3() {
            t = new Thread(this, "Thread_3");
            this.t.start();
        }

        public void run() {
            task_initialise();
            task();
            task_finalise();
        }

        public void task_initialise(){
            try{
                t.sleep(1500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task initialisation");

            Global.latch_1.countDown();
            Global.latch_2.countDown();
            Global.latch_4.countDown();
            Global.latch_5.countDown();

            try
            {
                Global.latch_3.await();

            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }

        public void task(){
            try{
                t.sleep(2000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task");
        }
        public void task_finalise(){
            try{
                t.sleep(2000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task_finalise");
            latch_1_2.countDown();
            latch_5.countDown();
        }
    }

    public static class Thread_4 implements Runnable {
        Thread t;
        Thread_4() {
            t = new Thread(this, "Thread_4");
            this.t.start();
        }

        public void run() {
            task_initialise();
            task();
            task_finalise();
        }

        public void task_initialise(){
            try{
                t.sleep(3000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task initialisation");

            Global.latch_1.countDown();
            Global.latch_2.countDown();
            Global.latch_3.countDown();
            Global.latch_5.countDown();

            try
            {
                Global.latch_4.await();

            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }

        public void task(){
            try{
                t.sleep(1000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task");
        }
        public void task_finalise(){
            try{
                t.sleep(2000);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task_finalise");

            latch_1_2.countDown();
            latch_5.countDown();
        }
    }
    public static class Thread_5 implements Runnable {
        Thread t;
        Thread_5() {
            t = new Thread(this, "Thread_5");
            this.t.start();
        }

        public void run() {
            task_initialise();
            task();
            task_finalise();
        }

        public void task_initialise(){
            try{
                t.sleep(500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }

            System.out.println(t.getName() + ": done task initialisation");

            Global.latch_1.countDown();
            Global.latch_2.countDown();
            Global.latch_3.countDown();
            Global.latch_4.countDown();

            try
            {
                Global.latch_5.await();

            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }

        public void task(){
            try{
                t.sleep(500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task");


            try
            {
                latch_5.await();
            }catch(InterruptedException e)
            {
                System.out.println(t.getName() + " interrupted");
            }
        }
        public void task_finalise(){
            try{
                t.sleep(500);
            }
            catch (InterruptedException e){
                {
                    System.out.println(t.getName() + " interrupted");
                }
            }
            System.out.println(t.getName() + ": done task_finalise");
        }
    }
}
