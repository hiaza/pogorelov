import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MyFileSystemTest {
    MyFileSystem system = new MyFileSystem();

    @org.junit.jupiter.api.Test
    void getCurrentElementName() {
        assertEquals("Home",system.getCurrentElementName(system.cursor));
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile1","artttttttttttem"));
        system.cursor = system.cursor.children.get(0);
        assertEquals("BineryFile1",system.getCurrentElementName(system.cursor));
    }

    @org.junit.jupiter.api.Test
    void getFiles() {
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile1","artttttttttttem"));
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile2","sdfsdfsdf"));
        system.createDirectory("Directory");
        List<String> result = new ArrayList<String>();
        result.add("BineryFile1");
        result.add("BineryFile2");
        result.add("Directory");
        assertEquals(system.getFiles().get(0),result.get(0));
        assertEquals(system.getFiles().get(1),result.get(1));
        assertEquals(system.getFiles().get(2),result.get(2));
    }

    @org.junit.jupiter.api.Test
    void createFile() {
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile1","artttttttttttem"));
        system.cursor = system.cursor.children.get(0);
        assertEquals("BineryFile1",system.getCurrentElementName(system.cursor));
        MyFileSystem.Readble temp = (MyFileSystem.Readble)system.cursor.data;
        assertEquals("artttttttttttem",temp.getContent());

        system.cursor = system.root;
        assertFalse(system.ReadFile());
        system.CreateFile(new MyFileSystem.LogFile("LogFile","This"));
        system.cursor = system.cursor.children.get(1);
        assertEquals("LogFile",system.getCurrentElementName(system.cursor));
        MyFileSystem.LogFile log = (MyFileSystem.LogFile)system.cursor.data;
        assertTrue(system.ReadFile());
        assertEquals("This",log.getContent());
        system.AppendLine("is me");
        assertEquals("This\nis me",log.getContent());
    }

    @org.junit.jupiter.api.Test
    void deleteFile() {
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile1","artttttttttttem"));
        system.CreateFile(new MyFileSystem.BinaryFile("BineryFile2","sdfsdfsdf"));
        system.createDirectory("Directory");
        List<String> result = new ArrayList<String>();
        result.add("BineryFile1");
        result.add("BineryFile2");
        result.add("Directory");
        assertEquals(system.getFiles().get(0),result.get(0));
        assertEquals(system.getFiles().get(1),result.get(1));
        assertEquals(system.getFiles().get(2),result.get(2));
        system.cursor = system.cursor.children.get(0);
        system.DeleteFile();
        system.getFiles();
        assertNotEquals(system.getFiles().get(0),result.get(0));
    }

    @org.junit.jupiter.api.Test
    void moveFile() {
        system.createDirectory("Directory");
        system.cursor = system.cursor.children.get(0);

        system.createDirectory("Directory Inside");
        system.CreateFile(new MyFileSystem.BinaryFile("temp","artttttttttttem"));
        system.CreateFile(new MyFileSystem.BinaryFile("rrwer","sdfsdfsdf"));

        system.cursor = system.cursor.children.get(0);
        system.CreateFile(new MyFileSystem.BinaryFile("Have to move","sdfsdfsdf"));
        system.CreateFile(new MyFileSystem.BinaryFile("temp","artttttttttttem"));
        system.cursor = system.cursor.children.get(0);
        system.Copy();
        system.cursor = system.root;
        system.MoveFile();
        assertEquals("Have to move",system.getFiles().get(1));
    }

    @org.junit.jupiter.api.Test
    void readFile() {
        system.createDirectory("Directory");
        system.CreateFile(new MyFileSystem.BinaryFile("temp","artttttttttttem"));
        system.CreateFile(new MyFileSystem.LogFile("xx","sdfsdfsdf"));
        system.cursor = system.cursor.children.get(0);
        assertFalse(system.ReadFile());
        system.cursor = system.root;
        system.cursor = system.cursor.children.get(1);
        assertTrue(system.ReadFile());
        system.cursor = system.root;
        system.cursor = system.cursor.children.get(2);
        assertTrue(system.ReadFile());
    }

    @org.junit.jupiter.api.Test
    void pushElement() {
        system.CreateFile(new MyFileSystem.BufferFile("buff"));
        system.CreateFile(new MyFileSystem.LogFile("log","here"));
        system.cursor = system.cursor.children.get(1);
        system.Copy();
        system.cursor = system.root;
        system.cursor = system.cursor.children.get(0);
        assertTrue(system.PushElement());
    }

    @org.junit.jupiter.api.Test
    void consumeElement() {
        system.CreateFile(new MyFileSystem.BufferFile("buff"));
        system.CreateFile(new MyFileSystem.LogFile("log","here"));
        system.cursor = system.cursor.children.get(1);
        system.Copy();
        system.cursor = system.root;
        system.cursor = system.cursor.children.get(0);
        assertTrue(system.PushElement());
        system.cursor = system.root;
        system.cursor = system.cursor.children.get(0);
        system.Copy();
        system.ConsumeElement();
        system.cursor = system.root;
        system.createDirectory("temp");
        system.cursor = system.cursor.children.get(2);
        system.MoveFile();
        assertEquals("log",system.getFiles().get(0));

    }

    @org.junit.jupiter.api.Test
    void createDirectory() {
        assertTrue(system.createDirectory("temp"));
        system.cursor = system.cursor.children.get(0);
        assertTrue(system.createDirectory("temp1"));
        system.CreateFile(new MyFileSystem.BufferFile("buff"));
        system.cursor = system.cursor.children.get(1);
        assertFalse(system.createDirectory("errr"));
    }

    @org.junit.jupiter.api.Test
    void deleteDirectory() {
        assertFalse(system.deleteDirectory());
        assertTrue(system.createDirectory("temp"));
        system.CreateFile(new MyFileSystem.BufferFile("buff"));
        assertEquals("temp",system.getFiles().get(0));
        system.cursor = system.cursor.children.get(0);
        assertTrue(system.deleteDirectory());
        assertNotEquals("temp",system.getFiles().get(0));
    }

    @Test
    void addConfig() {
        system.CreateFile(new MyFileSystem.ConfigurationFile("ConfFile"));
        system.cursor = system.cursor.children.get(0);
        system.addConfig("Users",10);
        system.addConfig("Admins",1);
        MyFileSystem.ConfigurationFile conf = (MyFileSystem.ConfigurationFile)system.cursor.data;
        assertEquals(10,conf.getConfig("Users"));
        assertEquals(1,conf.getConfig("Admins"));
        assertEquals(-1,conf.getConfig("Damn"));
    }

    @Test
    void deleteConfig() {
        system.CreateFile(new MyFileSystem.ConfigurationFile("ConfFile"));
        system.cursor = system.cursor.children.get(0);
        system.addConfig("Users",10);
        system.addConfig("Admins",1);
        MyFileSystem.ConfigurationFile conf = (MyFileSystem.ConfigurationFile)system.cursor.data;
        assertEquals(10,conf.getConfig("Users"));
        assertEquals(1,conf.getConfig("Admins"));
        system.deleteConfig("Admins");
        system.ReadFile();
        assertEquals(-1,conf.getConfig("Admins"));
    }
}